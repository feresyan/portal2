<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'part-head.php';?>
</head>
<body>

<!-- header -->
<header>
	<div class="container">
		<!-- nav -->
		<?php include 'part-navigation.php';?>
		<!-- //nav -->
	</div>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
</header>
<!-- //header -->

<!-- banner -->
<section class="banner_inner" id="home">
	<!--<div class="banner_inner_overlay"> -->
	<div style="background: url(<?php include 'php/coba.php'; ?>) no-repeat center;" class="banner_inner_overlay">
	</div>
</section>
<!-- //banner -->

<!-- Research Doc -->
<section class="packages pt-5">
	<div class="container py-lg-4 py-sm-3">
		<h1 class="heading text-capitalize text-center">Research Documents</h1>
		<p class="text mt-2 mb-5 text-center">These are our research documents from 6 labs on IRS. Please if you want to download them, please do not hesitate to contact us.</p>
		<?php 
			session_start();
			if (!empty($_SESSION['message'])){
				echo '<div class="alert alert-success text-center">'.$_SESSION["message"].'</div>';
				unset($_SESSION['message']);
			}
		?>
		<!--<input class="form-control" id="myInput" type="text" placeholder="Search.."><br>-->
		<div class="row">
			<div class="table-responsive">
				<table id="publication-table" class="table table-hover">
					<thead>
						<tr>
						<th scope="col">#</th>
						<th scope="col">Doc. Number</th>
						<th scope="col">Title</th>
						<th scope="col">Description</th>
						<th scope="col">Kategori</th>
						<th scope="col">Lab</th>
						<th scope="col">Author</th>
						<th scope="col">Action</th>
						</tr>
					</thead>

					<tbody id="myTable">
						<?php
							$conn = new mysqli("localhost", "isrdds", "labisr2018", "irsportal");
							if ($conn->connect_errno) {
								echo "Failed to connect to MySQL: " . $conn->connect_error;
							}
							$res = $conn->query("SELECT * , publication.id as idp, lab.alias as labname , type_doc.type as document, user_back.name as authorname FROM irsportal.publication INNER JOIN lab ON publication.idlab = lab.id INNER JOIN type_doc ON publication.idtype_doc = type_doc.id INNER JOIN user_back ON publication.idauthor = user_back.id;");
							while($row = $res->fetch_assoc()){
						?>
						<tr>
							<td><?php echo $row['idp'];?></td>
							<td><?php echo $row['doc_number'];?></td>
							<td><?php echo $row['title'];?></td>
							<td><?php echo $row['description'];?></td>
							<td><?php echo $row['document'];?></td>
							<td><?php echo $row['labname'];?></td>
							<td><?php echo $row['authorname'];?></td>
							<td>
								<?php 
									if ($row['restriction'] == "1" and $name = $_SESSION["name_session"]){
								?>
									<button type="button" class="btn btn-primary btn-sm" id="myButton" data-toggle="modal" data-target="#modalRequest" data-idp="<?php echo $row['idp'];?>" data-title="<?php echo $row['title'];?>">Request Print</button>
								<?php
									} else {
								?>
									<span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="This Document cannot be requested, You must login">
										<button id="myTooltip" type="button" class="btn btn-primary btn-sm" style="pointer-events: none;" disabled>Request Print</button>
									</span>
							</td>
								<?php
									}
								?>
						</tr>
						<?php 
							}
						?>
					</tbody>
				</table>
			</div>
 		</div>
 </div>
 
</section>
<!-- Research Doc -->

<!-- Modal -->
<div class="modal fade" id="modalRequest" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title">Request Print</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<form action="php/publication_request" method="post">
			<div class="modal-body">
				<div class="form-group">
					<label for="idp" class="col-form-label">Document Id:</label>
					<input type="text" class="form-control" id="idp" name="idp" readonly>
				</div>
				<div class="form-group">
					<label for="title" class="col-form-label">Document Title:</label>
					<input type="text" class="form-control" id="title" name="title" readonly>
				</div>
				<div class="form-group">
					<label for="message-text" class="col-form-label">Reason for Request:</label>
					<textarea class="form-control" id="message-text" name="reason"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<input type="submit" class="btn btn-primary" value="Send Request">
			</div>
		</form>
		</div>
	</div>
</div>
<!-- //Modal -->

<!--footer -->
<?php include 'part-footer.php';?>
<!-- //footer -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<!--
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
-->
<script>
$(document).ready(function () {
  $('#publication-table').DataTable();
});
</script>

<script>
$('#modalRequest').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var idp = button.data('idp')
  var title = button.data('title')
  document.getElementById("idp").value = idp
  document.getElementById("title").value = title
})
</script>

<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>

<!-- copyright -->
<div class="copyright py-3 text-center">
	<p>Create by Infrastructure Research & Standardization</p>
</div>
<!-- //copyright -->	
</body>
</html>