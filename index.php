<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'part-head.php';?>
</head>
<body>

<!-- header -->
<header>
	<div class="container">
		<!-- nav -->
		<?php include 'part-navigation.php';?>
		<!-- //nav -->
	</div>
</header>
<!-- //header -->

<!-- banner -->
<section class="banner_w3lspvt" id="home">
	<div class="csslider infinity" id="slider1">
		<input type="radio" name="slides" checked="checked" id="slides_1" />
		<input type="radio" name="slides" id="slides_2" />
		<input type="radio" name="slides" id="slides_4" />
		<ul>
			<li>
				<div style="background: url(<?php include 'php/coba.php'; ?>) no-repeat center;" class="banner-top">
					<div class="overlay">
						<div class="container">
							<div class="w3layouts-banner-info">
								<h3 class="text-wh"><?php include 'php/bnr_judul.php'; ?></h3>
								<h4 class="text-wh"><?php include 'php/bnr_teks.php'; ?></h4>
								<div class="buttons mt-4">
									<a href="research.php" class="btn mr-2"><?php include 'php/bnr_btn.php'; ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div style="background: url(<?php include 'php/coba2.php'; ?>) no-repeat center;" class="banner-top2">
					<div class="overlay">
						<div class="container">
							<div class="w3layouts-banner-info">
								<h3 class="text-wh"><?php include 'php/bnr_judul2.php'; ?></h3>
								<h4 class="text-wh"><?php include 'php/bnr_text2.php'; ?></h4>
								<div class="buttons mt-4">
									<a href="about.php" class="btn mr-2"><?php include 'php/bnr_btn2.php'; ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div style="background: url(<?php include 'php/coba3.php'; ?>) no-repeat center;" class="banner-top4">
					<div class="overlay1">
						<div class="container">
							<div class="w3layouts-banner-info">
								<h3 class="text-wh"><?php include 'php/bnr_judul3.php'; ?></h3>
								<h4 class="text-wh"><?php include 'php/bnr_text3.php'; ?></h4>
								<div class="buttons mt-4">
									<a href="about.php" class="btn mr-2"><?php include 'php/bnr_btn3.php'; ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</li>
		</ul>
		<div class="arrows">
			<label for="slides_1"></label>
			<label for="slides_2"></label>
			<label for="slides_4"></label>
		</div>
	</div>
</section>
<!-- //banner -->

<!-- about -->
<section class="about py-5">
	<div class="container py-lg-5 py-sm-4">
		<div class="row">
			<div class="col-lg-6 about-left">
				<h3 class="mt-lg-3"><strong><?php include 'php/title_news.php'; ?></strong></h3>
				<p class="mt-4"><?php include 'php/berita.php'; ?></p>
				<p class="mt-3"> Donec a arcu et sapien hendrerit accumsan. Pellentesque sit amet eros iac, elementum 
				urna ipsum accumsan, iaculis ligula. Aenean quam eget maximus in convallis felis dapibus sit amet.</p>
			</div>
			<div class="col-lg-6 about-right text-lg-right mt-lg-0 mt-5">
				<img src="<?php include 'php/gmbr_news.php'; ?>" alt="" class="img-fluid abt-image" />
			</div>
		</div>
		<div class="row mt-5 text-center">
			<div class="col-lg-3 col-6">
				<div class="counter">
					<span class="fa fa-smile-o"></span>
					<div class="timer count-title count-number"> <?php include 'php/counter.php'; echo $new_count; ?> </div>
					<p class="count-text text-uppercase">happy customers</p>
				</div>
			</div>
			<div class="col-lg-3 col-6">
				<div class="counter">
					<span class="fa fa-users"></span>
					<div class="timer count-title count-number"> <?php include 'php/jml_request.php'; ?> </div>
					<p class="count-text text-uppercase">Request</p>
				</div>
			</div>
			<div class="col-lg-3 col-6 mt-lg-0 mt-5">
				<div class="counter">
					<span class="fa fa-users"></span>
					<div class="timer count-title count-number"> <?php include 'php/jml_publication.php'; ?> </div>
					<p class="count-text text-uppercase">publication</p>
				</div>
			</div>
			<div class="col-lg-3 col-6 mt-lg-0 mt-5">
				<div class="counter">
					<span class="fa fa-gift"></span>
					<div class="timer count-title count-number"> <?php include 'php/jml_author.php'; ?></div>
					<p class="count-text text-uppercase">Author</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- //about -->

<!-- text -->
<section class="text-content">
	<div class="overlay-inner py-5">
		<div class="container py-md-3">
			<div class="test-info">
				<h4 class="tittle">Our Vision</h4>
				<p class="mt-3">Ensuring the effectiveness of end-to-end infrastructure research 
					and development activities in accordance with company strategy and planning.</p>
				<div class="text-left mt-4">
						<a href="about.php">More About Us</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- //text -->

<!-- destinations -->
<section class="destinations py-5" id="destinations">
	<div class="container py-xl-5 py-lg-3">
		<h3 class="heading text-capitalize text-center"> These Are Our Awesome Labs</h3>
		<p class="text mt-2 mb-5 text-center">We have 6 labs located on Bandung, each labs has different skill and expertise, so check us out here</p>
		<div class="row inner-sec-w3layouts-w3pvt-lauinfo justify-content-around">
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center">
				<h4 class="destination mb-3">Broadband Access Network</h4>
				<div class="image-position position-relative">
					<img src="images/BAN.jpg" class="img-fluid" alt="">
				</div>
				<div class="destinations-info">
					<div class="caption mb-lg-3">
						<h4>Broadband Access Network</h4>
						<a href="lab-bcn.php">Learn More</a>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center">
				<h4 class="destination mb-3">Broadband Core Network</h4>
				<div class="image-position position-relative">
					<img src="images/BCN.jpg" class="img-fluid" alt="">
				</div>
				<div class="destinations-info">
					<div class="caption mb-lg-3">
						<h4>Broadband Core Network</h4>
						<a href="lab-ban.php">Learn More</a>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center mt-md-0 mt-4">
				<h4 class="destination mb-3">Cloud & Node Platform</h4>
				<div class="image-position position-relative">
					<img src="images/CNP.jpg" class="img-fluid" alt="">
				</div>
				<div class="destinations-info">
					<div class="caption mb-lg-3">
						<h4>Cloud & Node Platform</h4>
						<a href="lab-cnp.php">Learn More</a>
					</div>
				</div>
			</div>
		</div>
	</br></br>
		<div class="row inner-sec-w3layouts-w3pvt-lauinfo justify-content-around">
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center mt-md-0 mt-4">
				<h4 class="destination mb-3">Mobility & FMC</h4></br>
				<div class="image-position position-relative">
					<img src="images/FMC.jpg" class="img-fluid" alt="">
				</div>
				<div class="destinations-info">
					<div class="caption mb-lg-3">
						<h4>Mobility & FMC</h4>
						<a href="lab-fmc.php">Learn More</a>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center mt-md-0 mt-4">
				<h4 class="destination mb-3">Infrastructure Service</h4>
				<div class="image-position position-relative">
					<img src="images/ISR.jpg" class="img-fluid" alt="">
				</div>
				<div class="destinations-info">
					<div class="caption mb-lg-3">
						<h4>Infrastructure Service</h4>
						<a href="lab-isr.php">Learn More</a>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-6 destinations-grids text-center mt-md-0 mt-4">
				<h4 class="destination mb-3">Security, OSS & BSS</h4>
				<div class="image-position position-relative">
					<img src="images/SOB.jpg" class="img-fluid" alt="">
				</div>
				<div class="destinations-info">
					<div class="caption mb-lg-3">
						<h4>Security, OSS & BSS</h4>
						<a href="lab-sob.php">Learn More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- destinations -->

<!-- how to book -->
<section class="book py-5">
	<div class="container py-lg-5 py-sm-3">
		<h2 class="heading text-capitalize text-center"> Our Shared Service</h2>
		<div class="row mt-5 text-center">
			<div class="col-lg-4 col-sm-6">
				<div class="grid-info">
					<div class="icon">
						<span class="fa fa-map-signs"></span>
					</div>
					<h4>Technical Support</h4>
					<p class="mt-3">We can assist employees in technical matters in the Division on Digital Service Telkom Bandung.</p>
				</div>
			</div>
			<div class="col-lg-4 col-sm-6 mt-sm-0 mt-5">
				<div class="grid-info">
					<div class="icon">
						<span class="fa fa-calendar"></span>
					</div>
					<h4>Request Research Document</h4>
					<p class="mt-3">We offer research result and standard documents produced by each lab at the IRS for free. But not all of them have public access.</p>
				</div>
			</div>
			<div class="col-lg-4 col-sm-6 mt-lg-0 mt-5">
				<div class="grid-info">
					<div class="icon">
						<span class="fa fa-gift"></span>
					</div>
					<h4>Prototype</h4>
					<p class="mt-3">We can have a trial and trying a new technology for you, we will deliver the result in a form of prototype.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- //how to book -->

<!--footer -->
<?php include 'part-footer.php';?>
<!-- //footer -->

<!-- copyright -->
<div class="copyright py-3 text-center">
	<p>Create by Infrastructure Research & Standardization</p>
</div>
<!-- //copyright -->

<!-- move top -->
<div class="move-top text-right">
	<a href="#home" class="move-top"> 
		<span class="fa fa-angle-up  mb-3" aria-hidden="true"></span>
	</a>
</div>
<!-- move top -->

</body>
</html>