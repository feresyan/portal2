<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'part-head.php';?>
</head>
<body>

<!-- header -->
<header>
	<div class="container">
		<!-- nav -->
		<?php include 'part-navigation.php';?>
		<!-- //nav -->
	</div>
</header>
<!-- //header -->

<!-- banner -->
<section class="banner_inner" id="home">
	<div class="banner_inner_overlay">
	<div style="background: url(<?php include 'php/coba.php'; ?>) no-repeat center;" class="banner_inner_overlay">	
	</div>
</section>
<!-- //banner -->

<!-- about -->
<section class="about py-5">
	<div class="container py-lg-5 py-sm-4">
		<div class="row align-items-center">
			<div class="col-lg-6 about-left">
				<h3 class="mt-lg-3">Broadband Access Network Lab</h3>
				<p class="mt-4">We are here to Leading the management of BROADBAND ACCESS NETWORK LAB functions to support performance achievement.</p>
				<p class="mt-3"> We have 3 lab members:
					<ol > 
						<li><p class="mt-3"><strong>I Gede Astawa</strong> as Manager</p></li>
						<li><p class="mt-3"><strong>Karina Mahari</strong> as Consumer Broadband Service Engineer</p></li>
						<li><p class="mt-3"><strong>H. Lili Tasli SE</strong> as Broadband Wireline System Engineer</p></li>
					</ol>
				</p>
			</div>
			<div class="col-lg-6 about-right text-lg-right mt-lg-0 mt-5">
				<img src="images/about2.jpg" alt="" class="img-fluid abt-image" />
			</div>
		</div>
	</div>
</section>
<!-- //about -->


<!-- tabs -->
<section class="choose" id="choose">
	<div class="overlay-all py-5">
		<div class="container py-lg-5 py-sm-4">
			<h2 class="heading text-capitalize text-center mb-lg-5 mb-4">Our Responsibilities</h2>
			<div class="edu-exp-grids">
					<section id="content1">
						<div class="row text-center">
							<div class="col-lg-4 col-md-6 inner-w3pvt-wrap">
								<div class="inner-sec-grid">
									<span class="fa fa-gift"></span>
									<h4 class="mt-md-4 mt-2">First,</h4>
									<p class="mt-3">Ensuring the development of BAN laboratory / testbed as miniature network operational for new service research activities and simultaneously for problem troubleshooting facilities in operational.<br /><br /><br /><br /></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 inner-w3pvt-wrap">
								<div class="inner-sec-grid">
									<span class="fa fa-gift"></span>
									<h4 class="mt-md-4 mt-2">Second,</h4>
									<p class="mt-3">Ensuring the development of BAN technology research to provide recommendations on technology and infrastructure roadmaps, recommendations for strategic plans for infrastructure development and recommendations to support the preparation of infrastructure development business plans.</p>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 inner-w3pvt-wrap">
								<div class="inner-sec-grid">
									<span class="fa fa-gift"></span>
									<h4 class="mt-md-4 mt-2">Third,</h4>
									<p class="mt-3">Ensuring alternative proposals for more prospective BAN architectural configurations in order to support infrastructure & network improvement activities.<br /><br /><br /><br /><br /><br /></p>
								</div>
							</div>
						</div>
					</section>
					<section id="content2">
						<div class="row text-center justify-content-md-center">
							<div class="col-lg-4 col-md-6 inner-w3pvt-wrap">
								<div class="inner-sec-grid">
									<span class="fa fa-gift"></span>
									<h4 class="mt-md-4 mt-2">Fourth,</h4>
									<p class="mt-3">Ensuring BAN planning to support the implementation of infrastructure development.<br /><br /><br /></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 inner-w3pvt-wrap">
								<div class="inner-sec-grid">
									<span class="fa fa-gift"></span>
									<h4 class="mt-md-4 mt-2">Fifth,</h4>
									<p class="mt-3">Ensure the assistance of BAN aspect (ISP & OSP) to support relevant units in the development and implementation of infrastructure.</p>
								</div>
							</div>
						</div>
					</section>
				
			</div>
		</div>
	</div>
</section>
<!-- tabs -->

<!--footer -->
<?php include 'part-footer.php';?>
<!-- //footer -->

<!-- copyright -->
<div class="copyright py-3 text-center">
	<p>© 2019 Grand Tour. All Rights Reserved | Design by <a href="http://w3layouts.com/" target="=_blank"> W3layouts </a></p>
</div>
<!-- //copyright -->

<!-- move top -->
<div class="move-top text-right">
	<a href="#home" class="move-top"> 
		<span class="fa fa-angle-up  mb-3" aria-hidden="true"></span>
	</a>
</div>
<!-- move top -->

	
</body>
</html>