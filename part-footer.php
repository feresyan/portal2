<footer>
<section class="footer footer_w3layouts_section_1its py-5">
	<div class="container py-lg-4 py-3">
		<div class="row footer-top">
			<div class="col-lg-3 col-sm-6 footer-grid_section_1its_w3">
				<div class="footer-title">
					<h3>Address</h3>
				</div>
				<div class="footer-text">
					<p>Location : DDS Building, 3th floor, St. Gegerkalong Hilir 47, Bandung</p>
					<p>Phone : +6222 4574784</p>
					<p>Email : <a href="mailto:admin@irsportalku.co.id">admin@irsportalku.co.id</a></p>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 footer-grid_section mt-sm-0 mt-4">
				<div class="footer-title">
					<h3>About Us</h3>
				</div>
				<div class="footer-text">
					<p>We are IRS (Infrastructure Research Standardization, one of unit in Digital Service Division on Telkom. Learn More about us!</p>
				</div>
				<ul class="social_section_1info">
					<li class="mb-2 facebook"><a href="#"><span class="fa fa-facebook"></span></a></li>
					<li class="mb-2 twitter"><a href="#"><span class="fa fa-twitter"></span></a></li>
					<li class="google"><a href="#"><span class="fa fa-google-plus"></span></a></li>
					<li class="linkedin"><a href="#"><span class="fa fa-linkedin"></span></a></li>
				</ul>
			</div>
			<div class="col-lg-6 col-sm-6 mt-lg-0 mt-4 footer-grid_section_1its_w3">
				<div class="footer-title">
					<h3>Here we Are on Maps</h3>
				</div>
				<!-- map -->	
				<div class="map p-2">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1980.5735855191588!2d107.58705243075644!3d-6.87296349950356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e690c8eef9b3%3A0xb46a9d0a7ce2d0ef!2sDivisi+Digital+Service+(formerly+Telkom+RisTI)!5e0!3m2!1sid!2sid!4v1557720161431!5m2!1sid!2sid" width="500" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<!-- //map -->
			</div>
		</div>
	</div>
</section>
</footer>

<!-- js -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!-- js -->