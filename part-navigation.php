<?php
session_start();
if (!isset($_SESSION["name_session"])) {
	$name = "Member Area";
} else {
	$name = $_SESSION["name_session"];
	$name = "Halo, " . $name;
	$email = $_SESSION["email_session"];
	$password = $_SESSION["password_session"];
}
?>

<nav class="py-md-4 py-3 d-lg-flex">
	<div id="logo">
		<h1 class="mt-md-0 mt-2"> <a href="/"><span class="fa fa-map-signs"></span> IRS Portal </a></h1>
	</div>
	<label for="drop" class="toggle"><span class="fa fa-bars"></span></label>
	<input type="checkbox" id="drop" />
	<ul class="menu ml-auto mt-1">
		<li class="active"><a href="/">Home</a></li>
		<li class="active"><a href="about.php">About IRS</a></li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Our Labs</a>
			<div class="dropdown-menu">
				<a class="dropdown-item" href="lab-ban">Broadband Access Network</a>
				<a class="dropdown-item" href="lab-bcn">Broadband Core Network</a>
				<a class="dropdown-item" href="lab-cnp">Cloud & Node Platform</a>
				<a class="dropdown-item" href="lab-fmc">Mobility & FMC</a>
				<a class="dropdown-item" href="lab-isr">Infrastructure Service</a>
				<a class="dropdown-item" href="lab-sob">Security, OSS & BSS</a>
			</div>
		</li>
		<li class="active"><a href="research">Publication</a></li>
		<li class="active"><a href="contact">Contact</a></li>
		<li class="dropdown">
			<!--div class="dropdown-menu"-->
				<!--button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
					<span class="fa fa-user"></span> </button-->
				<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $name; ?></a>
				<div class="dropdown-menu">
					<?php
					if (!isset($_SESSION["name_session"])) {
						echo '<a class="dropdown-item" href="login">Login/Register</a>';
						//echo '<label class="dropdown-item">Logout</label>';
					} else {
						//echo '<label class="dropdown-item">Login</label>';
						echo '<a class="dropdown-item" href="php/logout">Logout</a>';
					}
					?>
				</div>
			<!--/div-->
		</li>
		<!--li class=""><a href="#"</a></li-->
		<!--<li class="booking"><a href="booking.html">Login</a></li>-->
	</ul>
</nav>
