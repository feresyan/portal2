$(document).ready(function () {

    $('#login_reg').on('click', function (e) {

        e.preventDefault();
        e.stopImmediatePropagation();

        var email = $("#inputEmail").val();
        var password = $("#inputPassword").val();

        var dataString = "email=" + email + "&password=" + password;
        alert(dataString);

        $.ajax({
            type: "POST",
            url: "http://localhost/php/loginbook.php",
            data: dataString,
            crossDomain: true,
            cache: false,
            beforeSend: function () {
                alert('Lanjut proses..!');
            },
            success: function (data) {
                if (data == 'success') {
                    alert('Login Success..!');
                    $("#inputEmail").val('');
                    $("#inputPassword").val('');
                    window.location.href = 'http://localhost/login.html';
                    //$.mobile.changePage('#pgCekPickup', {transition: 'slide'}); // jquery mobile
                } else if (data == 'error') {
                    alert('Login Failed...!');
                }
            },
            error: function (data) {
                alert('Error');
            }
        });

    });

});