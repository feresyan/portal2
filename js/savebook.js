$(document).ready(function () {

    // Tempat menyimpan pendaftaran
    // ------------------------------
    $('#save_reg').on('click', function (e) {

        e.preventDefault();
        e.stopImmediatePropagation();

        var name = $("#user-name").val();
        var email = $("#user-email").val();
        var password = $("#user-pass").val();
        var repeatpass = $("#user-repeatpass").val();
        
        if(password==repeatpass) {
        var dataString = "name=" + name + "&email=" + email + "&password=" + password;
        alert(dataString);

        $.ajax({
            type: "POST",
            url: "http://irsportal.infrasvc.id/php/register.php",
            data: dataString,
            crossDomain: true,
            cache: false,
            beforeSend: function () {
                alert('Lanjut proses ...');
            },
            success: function (data) {
                if (data == 'success') {
                    alert('Tambah data berhasil');
                    $("#user-name").val('');
                    $("#user-email").val('');
                    $("#user-pass").val('');
                    $("#user-repeatpass").val('');
                    window.location.href = "http://irsportal.infrasvc.id/login.html";
                    //$.mobile.changePage('#pgCekPickup', {transition: 'slide'}); // jquery mobile
                } else if (data == 'error') {
                    alert('Maaf, gagal tambah data');
                }
            },
            error: function (data) {
                alert('Error');
            }
        });
        } else {
            alert('Password and Repeat Password, NO MATCH');
        }

    });

    // Tempat membaca user login ada atau tidak ada
    // ---------------------------------------------
    $('#login_reg').on('click', function (e) {

        e.preventDefault();
        e.stopImmediatePropagation();

        var email = $("#inputEmail").val();
        var password = $("#inputPassword").val();

        var dataString = "email=" + email + "&password=" + password;
        //alert(dataString);

        $.ajax({
            type: "POST",
            url: "http://irsportal.infrasvc.id/php/loginbook.php",
            data: dataString,
            crossDomain: true,
            cache: false,
            beforeSend: function () {
                alert('Lanjut proses sebagai 'email'...!');
            },
            success: function (data) {
                if (data == 'success') {
                    alert('Login Success sebagai 'email'..!');
                    $("#inputEmail").val('');
                    $("#inputPassword").val('');
                    window.location.href = "http://irsportal.infrasvc.id/index.php";
                    //$.mobile.changePage('#pgCekPickup', {transition: 'slide'}); // jquery mobile
                } else if (data == 'error') {
                    alert('Login Failed sebagai 'email'..!');
                }
            }
        });

    });


    // Tempat reset password di sini
    // ---------------------------------------------
    $('#reset_pass').on('click', function (e) {

        e.preventDefault();
        e.stopImmediatePropagation();

        var email = $("#resetEmail").val();

        var dataString = "email=" + email;
        //alert(dataString);

        $.ajax({
            type: "POST",
            url: "http://irsportal.infrasvc.id/php/resetpass.php",
            data: dataString,
            crossDomain: true,
            cache: false,
            beforeSend: function () {
                alert('Lanjut proses..!');
            },
            success: function (data) {
                if (data == 'success') {
                    alert('Reset Password Success..!');
                    $("#resetEmail").val('');
                    window.location.href = 'http://irsportal.infrasvc.id/login.html';
                    //$.mobile.changePage('#pgCekPickup', {transition: 'slide'}); // jquery mobile
                } else if (data == 'error') {
                    alert('Email Failed, please try again..!');
                }
            }
        });

    });

});