<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'part-head.php';?>
</head>
<body>

<!-- header -->
<header>
	<div class="container">
		<!-- nav -->
		<?php include 'part-navigation.php';?>
		<!-- //nav -->
	</div>
</header>
<!-- //header -->

<!-- banner -->
<section class="banner_inner" id="home">
	<div class="banner_inner_overlay">
	<div style="background: url(<?php include 'php/coba.php'; ?>) no-repeat center;" class="banner_inner_overlay">		
	</div>
</section>
<!-- //banner -->

<!-- about -->
<section class="about py-5">
	<div class="container py-lg-5 py-sm-4">
		<div class="row align-items-center">
			<div class="col-lg-6 about-left">
				<h3 class="mt-lg-3">We are IRS</h3>
				<p class="mt-4">We are here to ensuring the effectiveness of end-to-end 
					infrastructure research and development activities in accordance with company strategy and planning.</p>
				<p class="mt-3"> Some of our activity:
					<ol > 
						<li><p class="mt-3">Standardization: Development of telecommunication system standards and platforms.</p></li>
						<li><p class="mt-3">Expertise Help on Digital Infra: Providing expertise in the field of telecommunication structures (BANTEK).</p></li>
						<li><p class="mt-3">Technology Research on Digital Infra: Do research on infrastructure technology that will be applied in TELKOM.</p></li>
						<li><p class="mt-3">Shared Service for Amoeba: Supporting the needs of amoeba related to infrastructure technology.</p></li>
						<li><p class="mt-3">Joint Research: Do joint research on new technology.</p></li>
					</ol>
				</p>
			</div>
			<div class="col-lg-6 about-right text-lg-right mt-lg-0 mt-5">
				<img src="images/about2.jpg" alt="" class="img-fluid abt-image" />
			</div>
		</div>
	</div>
</section>
<!-- //about -->


<!-- tabs -->
<section class="choose" id="choose">
	<div class="overlay-all py-5">
		<div class="container py-lg-5 py-sm-4">
			<h2 class="heading text-capitalize text-center mb-lg-5 mb-4">Our Responsibilities</h2>
			<div class="edu-exp-grids">
					<section id="content1">
						<div class="row text-center">
							<div class="col-lg-4 col-md-6 inner-w3pvt-wrap">
								<div class="inner-sec-grid">
									<span class="fa fa-gift"></span>
									<h4 class="mt-md-4 mt-2">First,</h4>
									<p class="mt-3"> <?php include 'php/forum.php'; ?> <br /><br /></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 inner-w3pvt-wrap">
								<div class="inner-sec-grid">
									<span class="fa fa-gift"></span>
									<h4 class="mt-md-4 mt-2">Second,</h4>
									<p class="mt-3"> <?php include 'php/forum2.php'; ?> <br /><br /><br /></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 inner-w3pvt-wrap">
								<div class="inner-sec-grid">
									<span class="fa fa-gift"></span>
									<h4 class="mt-md-4 mt-2">Third,</h4>
									<p class="mt-3"> <?php include 'php/forum3.php'; ?> </p>
								</div>
							</div>
						</div>
					</section>
					<section id="content2">
						<div class="row text-center justify-content-md-center">
							<div class="col-lg-4 col-md-6 inner-w3pvt-wrap">
								<div class="inner-sec-grid">
									<span class="fa fa-gift"></span>
									<h4 class="mt-md-4 mt-2">Fourth,</h4>
									<p class="mt-3"> <?php include 'php/forum4.php'; ?> <br /><br /><br /></p>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 inner-w3pvt-wrap">
								<div class="inner-sec-grid">
									<span class="fa fa-gift"></span>
									<h4 class="mt-md-4 mt-2">Fifth,</h4>
									<p class="mt-3"> <?php include 'php/forum5.php'; ?> </p>
								</div>
							</div>
						</div>
					</section>
			</div>
		</div>
	</div>
</section>
<!-- tabs -->

<!--footer -->
<?php include 'part-footer.php';?>
<!-- //footer -->

<!-- copyright -->
<div class="copyright py-3 text-center">
	<p>Create by Infrastructure Research & Standardization</p>
</div>
<!-- //copyright -->

<!-- move top -->
<div class="move-top text-right">
	<a href="#home" class="move-top"> 
		<span class="fa fa-angle-up  mb-3" aria-hidden="true"></span>
	</a>
</div>
<!-- move top -->

	
</body>
</html>